package ru.t1.sukhorukova.tm.api.service;

import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    Project create(final String userId, String name, String description);

    Project updateById(final String userId, String id, String name, String description);

    Project updateByIndex(final String userId, Integer index, String name, String description);

    Project changeProjectStatusById(final String userId, String id, Status status);

    Project changeProjectStatusByIndex(final String userId, Integer index, Status status);

    boolean existsById(final String userId, String id);

}
