package ru.t1.sukhorukova.tm.model;

import ru.t1.sukhorukova.tm.api.model.IWBS;
import ru.t1.sukhorukova.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public final class Task extends AbstractUserOwnerModel implements IWBS {

    private String name = "";
    private String description = "";
    private Status status = Status.NOT_STARTED;
    private String projectId;
    private Date created = new Date();

    public Task() {

    }

    public Task(final User user, final String name, final String description, final Status status) {
        if (user == null) return;
        this.setUserId(user.getId());
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(final Status status) {
        this.status = status;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }
}
